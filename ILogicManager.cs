﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Text;
using ViewDB;

namespace WCF_ServiceLibrary_Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ILogicManager
    {
        [OperationContract]
        string GetMainSecToken(string inputedSecurityCode);

        [OperationContract]
        bool CheckConnectionToken(string SecToken);


        //═════════════════════ User Functions ═════════════════════\\

        [OperationContract]
        bool SignUp(string username, string password, string email, string firstName, string lastName, int age);

        [OperationContract]
        User Login(string username, string password);

        [OperationContract]
        bool DeleteUser(string username, string passwrod);

        [OperationContract]
        bool UpdateUserById(User user);

        ////////// The next function req security token OR password \\\\\\\\\\

        [OperationContract]
        User GetUserByID(int id, string SecToken);

        [OperationContract]
        User GetUserByName(string username, string SecToken);

        [OperationContract]
        int GetMaxUserId(string SecToken);

        [OperationContract]
        UserList GetUserByAge(int age, string SecToken);

        [OperationContract]
        UserList GetAllUsers(string SecToken);

        [OperationContract]
        UserList GetUsersByNameContains(string content, string SecToken);

        //Statistics
        [OperationContract]
        UserList GetTopThreeYoungestUsers(string SecToken);

        //Statistics
        [OperationContract]
        UserList GetTopThreeOldestUsers(string SecToken);

        //Statistics
        [OperationContract]
        UserList GetTopThreeUsersWithTheMostPlaces(string SecToken);

        [OperationContract]
        bool CheckEmailExists(string email);

        [OperationContract]
        bool CheckConfiramtionCode(string email, string confirmationCode);

        [OperationContract]
        bool ResetPassword(string email, string confirmationCode, string newPassword);

        //═════════════════════ User Functions ═════════════════════\\

        //═════════════════════ Place Functions ═════════════════════\\

        [OperationContract]
        bool CreatePlace(string placeName, double placeLong, double placeLatit, string userName, string userPassword);

        [OperationContract]
        bool CreatePlaceWithoutUser(string placeName, double placeLong, double placeLatit, int userId, string securityToken);

        [OperationContract]
        bool DeletePlaceByUserIdAndPlaceName(string placeName, int userId);

        ////////// The next function req security token OR password \\\\\\\\\\

        [OperationContract]
        Place GetPlaceById(int id, string passwordOrToken);

        [OperationContract]
        bool UpdatePlaceById(Place place, string passwordOrToken);

        [OperationContract]
        PlacesList GetPlacesByUserId(int id, string passwordOrToken);

        [OperationContract]
        PlacesList GetAllPlaces(string SecToken);

        [OperationContract]
        PlacesList GetPlaceByNameContains(string content, string SecToken);

        [OperationContract]
        PlacesList GetPlaceByUserNameContains(string content, string SecToken);

        [OperationContract]
        int GetMaxPlaceId(string SecToken);

        //═════════════════════ Place Functions ═════════════════════\\
    }

    [DataContract]
    public class OutputMsg
    {
        //setting v
        bool success = true;
        string messageData = "";
        string suggestion = "";


        [DataMember]
        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        [DataMember]
        public string MessageData
        {
            get { return messageData; }
            set { messageData = value; }
        }

        [DataMember]
        public string Suggestion
        {
            get { return suggestion; }
            set { suggestion = value; }
        }
    }

           
}
