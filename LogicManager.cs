﻿using Models;
using ViewDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Channels;
using System.Threading;
using static System.Net.Mime.MediaTypeNames;
using System.IO;
using System.Diagnostics;
using System.Runtime.Remoting.Contexts;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;

namespace WCF_ServiceLibrary_Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class LogicManager : ILogicManager
    {
        //setting v
        private UserList users; //Current connected users to the server

        private UserDB userDB; //For data base functions.
        private PlaceDB placeDB; //For data base functions.

        private string securityCode; //To get the mainToken the admin need to verify he have access to the server.
        private string mainToken; //Security token. With the Secutiry token, the admin can access vital data.



        public LogicManager()
        {
            users = new UserList();

            placeDB = new PlaceDB();
            userDB = new UserDB();

            mainToken = SharedSecurityCode.CurrentSecurityToken;
        }


        /// <summary>
        /// This function will compatre the inputed security code to the current security code in the server.
        /// If its the same the user will get full admin access.
        /// </summary>
        /// <param name="inputedSecurityCode"> Inputed security code </param>
        /// <returns> Security Token / Nothing </returns>
        public string GetMainSecToken(string inputedSecurityCode)
        {
            if (inputedSecurityCode.CompareTo(SharedSecurityCode.CurrentSecurityCode) == 0)
            {
                return mainToken;
            }
            return "";
        }

        /// <summary>
        /// This function will return true if the server connected to the admin client when the
        /// security token is the same.
        /// </summary>
        /// <param name="SecToken">The security password</param>
        /// <returns></returns>
        public bool CheckConnectionToken(string SecToken)
        {
            if (SecToken.CompareTo(mainToken) == 0)
            {
                return true;
            }
            return false;
        }

        //═════════════════════ User Functions ═════════════════════\\

        /// <summary>
        /// This function will create new user based on the given information from the client
        /// </summary>
        /// <param name="username">The users' name</param>
        /// <param name="password">The users' password</param>
        /// <param name="email">The users' email</param>
        /// <param name="firstName">The users' first name</param>
        /// <param name="lastName">The users' last name</param>
        /// <param name="age">The users' age</param>
        /// <returns>The output of the function -> (Success == true / Success == false)</returns>
        public bool SignUp(string username, string password, string email, string firstName, string lastName, int age)
        {
            //setting v
            User user = new User(username, password, email, firstName, lastName, age);// The new user

            return userDB.CreateUser(user);
        }

        /// <summary>
        /// This function will login the user base on the username and password that the client send
        /// </summary>
        /// <param name="username">The users' name</param>
        /// <param name="password">The users' password</param>
        /// <returns>The output of the function -> (Success == true / Success == false)</returns>
        public User Login(string username, string password)
        {
            if (userDB.ConfirmUserPasswordByName(username, password))
            {
                return userDB.GetUser(username);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// This function will delete the user by his name
        /// </summary>
        /// <param name="username"></param>
        /// <param name="passwrod"></param>
        /// <returns></returns>
        public bool DeleteUser(string username, string passwrod)
        {
            if(!userDB.ConfirmUserPasswordByName(username,passwrod))
            {
                return false;
            }

            return userDB.DeleteUser(username);          
        }
        /// <summary>
        /// This function will update the user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UpdateUserById(User user)
        {
            if (userDB.GetUser(user.Id) == null)
            {
                return false;
            }

            if (userDB.GetUser(user.UserName) != null)
            {
                if (!userDB.CompareUserIdToName(user.Id, user.UserName))
                {
                    return false;
                }
            }

            return userDB.UpdateUser(user);
        }

        public bool CheckEmailExists(string email)
        {
            if(userDB.GetUserByEmail(email) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CreateConfirmationCode(string email)
        {
            if(!IsValidEmail(email))
            {
                return false;
            }
            if (CheckEmailExists(email))
            {
                string confirmationCode = GeneratePassResetCode();
                SendMail(email, confirmationCode);
                SharedSecurityCode.EmailsForPasswordRestor.Add(new string[] { email, confirmationCode });
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidEmail(string email)
        {
            // Regular expression pattern for validating email addresses
            string pattern = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
            Regex regex = new Regex(pattern);

            return regex.IsMatch(email);
        }

        private string GeneratePassResetCode()
        {
            //setting v
            Random random = new Random();
            const string chars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, 6).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private async void SendMail(string email, string verificationCode)
        {
            string fromMail = "lfm.floppascience@gmail.com";
            string fromPassword = "xwel olxz yxvj ppcg";

            MailMessage message = new MailMessage
            {
                From = new MailAddress(fromMail),
                Subject = "Look For Me - Password Reset",
                Body = "Hello, In order to <strong>reset your password</strong> we need to make sure that we are talking with you.<br>Please enter this code <u>in order to reset your password:</u><br><strong><span style=\"font-size: 18px;\">" + verificationCode + "</span></strong>",
                IsBodyHtml = true

            };

            message.ReplyToList.Add("noreply.floppascience@gmail.com");

            message.To.Add(new MailAddress(email));

            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(fromMail, fromPassword),
                EnableSsl = true

            };

            smtpClient.Send(message);


        }

        public bool CheckConfiramtionCode(string email, string confirmationCode)
        {
            foreach (var emails in SharedSecurityCode.EmailsForPasswordRestor)
            {
                if (emails[0] == email)
                {
                    if (emails[1] == confirmationCode)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool ResetPassword(string email,string confirmationCode, string newPassword) 
        {
            if (CheckConfiramtionCode(email, confirmationCode))
            {
                User user = userDB.GetUserByEmail(email);
                user.Password = newPassword;
                userDB.UpdateUser(user);
                return true;
            }
            else
            {
                return false;
            }
        }

        ////////// The next function req security token OR password \\\\\\\\\\

        /// <summary>
        /// This function will return the user from his id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUserByID(int id, string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return userDB.GetUser(id);
        }

        public User GetUserByName(string username, string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return userDB.GetUser(username);
        }

        public int GetMaxUserId(string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return 0; }

            return userDB.GetMaxUserId();
        }

        /// <summary>
        /// This function will return the users from their age
        /// </summary>
        /// <param name="age"></param>
        /// <param name="SecToken"></param>
        /// <returns></returns>
        public UserList GetUserByAge(int age, string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return userDB.GetUsersByAge(age);
        }

        /// <summary>
        /// This function will return all the users in the data base
        /// </summary>
        /// <param name="SecToken"></param>
        /// <returns></returns>
        public UserList GetAllUsers(string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return userDB.GetAllUsers();
        }

        /// <summary>
        /// This function will return the users by their name contains
        /// </summary>
        /// <param name="content"></param>
        /// <param name="SecToken"></param>
        /// <returns></returns>
        public UserList GetUsersByNameContains(string content, string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return userDB.GetUsersByNameContains(content);
        }

        //Statistics
        public UserList GetTopThreeYoungestUsers(string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return userDB.GetTop3Youngest();
        }

        //Statistics
        public UserList GetTopThreeOldestUsers(string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return userDB.GetTop3Oldest();
        }

        //Statistics
        public UserList GetTopThreeUsersWithTheMostPlaces(string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return userDB.GetTop3WithMostPlaces();
        }

        //═════════════════════ User Functions ═════════════════════\\

        //═════════════════════ Place Functions ═════════════════════\\

        public bool CreatePlace(string placeName, double placeLong, double placeLatit, string userName, string userPassword)
        {
            Place place = new Place()
            {
                PlaceName = placeName,
                PlaceLongitude = placeLong,
                PlaceLatitude = placeLatit,
            };

            if (userDB.ConfirmUserPasswordByName(userName, userPassword))
            {
                int id = userDB.GetUser(userName).Id;
                place.UserId = id;
                return placeDB.CreatePlace(place);
            }
            return false;
        }

        public bool CreatePlaceWithoutUser(string placeName, double placeLong, double placeLatit, int userId, string securityToken)
        {
            if (!CheckConnectionToken(securityToken)) { return false; }

            Place place = new Place()
            {
                PlaceName = placeName,
                PlaceLongitude = placeLong,
                PlaceLatitude = placeLatit,
                UserId = userId
            };
            return placeDB.CreatePlace(place);
        }

        public bool DeletePlaceByUserIdAndPlaceName(string placeName, int userId)
        {
            return placeDB.DeletePlace(userId, placeName);
        }

        



        ////////// The next function req security token OR password \\\\\\\\\\

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Place GetPlaceById(int id, string passwordOrToken)
        {
            if (!CheckConnectionToken(passwordOrToken)) 
            {
                if (!userDB.ConfirmUserPasswordById(id, passwordOrToken)) 
                { 
                    return null; 
                }
            }

            return placeDB.GetPlace(id);
        }

        public bool UpdatePlaceById(Place place, string passwordOrToken)
        {
            if (!CheckConnectionToken(passwordOrToken))
            {
                if (!userDB.ConfirmUserPasswordById(place.Id, passwordOrToken))
                {
                    return false;
                }
            }
            return placeDB.UpdatePlaceById(place);
        }

        public PlacesList GetPlacesByUserId(int id, string passwordOrToken)
        {
            if (!CheckConnectionToken(passwordOrToken))
            {
                if (!userDB.ConfirmUserPasswordById(id, passwordOrToken))
                {
                    return null;
                }
            }

            return placeDB.GetPlacesByUser(id);
        }

        public PlacesList GetAllPlaces(string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return placeDB.GetAllPlaces();
        }

        public PlacesList GetPlaceByNameContains(string content, string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return placeDB.GetPlaceByNameContains(content);
        }

        public PlacesList GetPlaceByUserNameContains(string content, string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return null; }

            return placeDB.GetPlaceByUserNameContains(content);
        }

        

        public int GetMaxPlaceId(string SecToken)
        {
            if (!CheckConnectionToken(SecToken)) { return 0; }

            return placeDB.GetMaxPlaceId();
        }

        //═════════════════════ Place Functions ═════════════════════\\

    }
}
